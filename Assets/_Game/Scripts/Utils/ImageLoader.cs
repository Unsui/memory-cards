﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.Utils;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json.Linq;


namespace Game
{
    
[System.Serializable]
public class ImageLoader : MonoBehaviour
{
   [SerializeField] private Card[] _cardsArray;
   private List<Texture2D> _textures = new List<Texture2D>();
   private string url_Json = "https://drive.google.com/uc?export=view&id=1ZE_Rj2ChOvzh_ZQDXwGSGvckmBw3Pxyu";
   
   
      void Start()
      {
          
          GameManager.Instance.WaitTextureLoad = true;
          StartCoroutine(GetJsonFromWeb()); 
      }
  
      IEnumerator GetImageFromWeb(string front_url)
      {
          
          UnityWebRequest reg = UnityWebRequestTexture.GetTexture(front_url);
          yield return reg.SendWebRequest();
          if (reg.isNetworkError || reg.isHttpError)
          {
              var info = "  Image load error";
              GameManager.Instance.SetInfoText(info);
          }
          else
          {
              Texture2D img = ((DownloadHandlerTexture) reg.downloadHandler).texture;
              _textures.Add(img);
          }
      }

      IEnumerator GetJsonFromWeb()
      {
          
          if (Application.internetReachability == NetworkReachability.NotReachable)
          {
              //debug
              var debug = "NetWork Status: Not Reachable.";
              GameManager.Instance.SetInfoText(debug);
          }
          
          
          UnityWebRequest reg = UnityWebRequest.Get(url_Json);
          yield return reg.SendWebRequest();
          if (reg.isNetworkError || reg.isHttpError)
          {
              //debug
              Debug.Log("  get Json error");
              var info = "isNetworkError  get Json";
              GameManager.Instance.SetInfoText(info);
          }

          var json = reg.downloadHandler.text;
          JObject jObject = JObject.Parse(json);
          
          IList<JToken> results = jObject["PicLinksArray"].Children().ToList();
          IList<CardPath> cardPaths = new List<CardPath>();
          
          foreach (JToken token in results)
          {
              CardPath cardPath = token.ToObject<CardPath>();
              cardPaths.Add(cardPath);
              
              
          }
          
          GetImgList(cardPaths);
      }

      private async void GetImgList(IList<CardPath> pathList)
      {
          foreach (var path in pathList)
          {
              StartCoroutine(GetImageFromWeb(path.path)); 
          }
          
          await UniTaskUtils.Delay(3);
          GameManager.Instance.LoadImgsList(_textures);
      }
}

}
