﻿using System;
using UnityEngine;


namespace Game.Utils
{
    
    public static class LocalSettings
    {
        private const string MatchGotQtyKey = "StarsGotQty";
        private const string IsMusicEnabledKey = "IsMusicEnabled";
        private const string IsSoundsEnabledKey = "IsSoundsEnabled";
        private const string VersionKey = "Version";
    
  
        public static float CurrentVersion
        {
            get => PlayerPrefs.GetFloat(VersionKey, 1.0f);
            set => PlayerPrefs.SetFloat(VersionKey, value);
        }
        
        public static int MatchGotQty
        {
            get => PlayerPrefs.GetInt(MatchGotQtyKey, -2);
            set => PlayerPrefs.SetInt(MatchGotQtyKey, value);
        }
    
        //Sound
        public static bool IsMusicEnabled
        {
            get => PrefsUtils.GetBool(IsMusicEnabledKey, true);
            set => PrefsUtils.SetBool(IsMusicEnabledKey, value);
        }
    
        public static bool IsSoundsEnabled
        {
            get => PrefsUtils.GetBool(IsSoundsEnabledKey, true);
            set => PrefsUtils.SetBool(IsSoundsEnabledKey, value);
        }
  
       
    }

}