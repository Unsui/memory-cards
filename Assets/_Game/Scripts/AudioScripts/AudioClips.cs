﻿  
namespace Game
{
    
    public static class AudioClips
    {
        // ===== Game ===== 
        
        public const string MainTrack = "background";
        public const string SessionEnd = "session_end";
        public const string Match = "match";
        public const string Select = "select";
        
       
        
        // ===== UI ===== 

        public const string ButtonClick = "tap_btn";
        
    }

}


