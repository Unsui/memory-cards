﻿using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using Game.Utils;
using UnityEngine.Android;
using Random = UnityEngine.Random;


namespace Game
{
    public class GameManager : SingletonT<GameManager>
    {
        public static event Action sessionDoneAction;
        [SerializeField] private Card[] _cardArray;
        [SerializeField] private TextMeshProUGUI _scoreText;
        [SerializeField] private TextMeshProUGUI _infoText;
        [SerializeField] private int _waitSec;

        private Card _firstCard;
        private List<Texture2D> _texture2Ds;
        private int[] _randomArray;
        private int _matchesQty = 0;
        private bool _waitSecondCard;
        public GameState GameState{ get; set;}
        public bool WaitTextureLoad{get; set;}

        void Start()
        {
            _randomArray = new int[_cardArray.Length];
            GameState = GameState.None;
            AudioSystem.Instance.PlayMusic(AudioClips.MainTrack);
        }

        private void ClearRandomArray()
        {
            for (int j = 0; j < _randomArray.Length; j++)
            {
                _randomArray[j] = -1;
            }
        }
        
        public async void StartNewSession()
        {
            if (WaitTextureLoad)
            {
                return;
            }
            
            ClearRandomArray();
            FillRandomArray();
            
            //Put texture on card face
            for (int i = 0; i < _cardArray.Length; i++)
            {
                var img = _texture2Ds[_randomArray[i]];
                _cardArray[i].SetFaceTexture(img);
                _cardArray[i].ImageNumber = _randomArray[i];
            }
            
            _matchesQty = 0;
            SetScoreText(_matchesQty);
            GameState = GameState.Playing;
            
            foreach (var card in _cardArray)
            {
                card.ShowFace();
            }
            AudioSystem.Instance.PlayOneShot(AudioClips.Select);

            await UniTaskUtils.Delay(_waitSec);
            
            foreach (var card in _cardArray)
            {
                card.ShowBackside();
            }
            
            AudioSystem.Instance.PlayOneShot(AudioClips.Select);
        }

        public void StopSession()
        {
            foreach (var card in _cardArray)
            {
                card.ShowBackside();
                card.IsMatched = false;
                _firstCard = null;
                _waitSecondCard = false;
            }

            _matchesQty = 0;
            SetScoreText(_matchesQty);
            GameState = GameState.None;
            sessionDoneAction?.Invoke();
        }
        
        public void SessionDone()
        {
            AudioSystem.Instance.PlayOneShot(AudioClips.SessionEnd);
            
            LocalSettings.MatchGotQty = _matchesQty;
            // sessionDoneAction?.Invoke();
        }
        
        public void MatchCollected()
        {
            _matchesQty++;
            SetScoreText(_matchesQty);
        }

        void SetScoreText(int score)
        {
            _scoreText.text = $"{score}";
        }
        
        public void SetInfoText(string info)
        {
            _infoText.text = $"{info}";
        }

        public void LoadImgsList(List<Texture2D> textureList)
        {
            _texture2Ds = textureList;
            WaitTextureLoad = false;
        }
        
        private void FillRandomArray()
        {
            var numberDoneList = new List<int>();
            bool _isNumPlaced;
            int randomNum;
            
            for (int i = 0; i < _randomArray.Length; i++)
            {
                randomNum = Random.Range(0, _texture2Ds.Count);
                _isNumPlaced = false;
                
                while (!_isNumPlaced)
                {
                    randomNum = Random.Range(0, _texture2Ds.Count);
                
                    if (!numberDoneList.Contains(randomNum))
                    {
                        if (!_randomArray.Contains(randomNum))
                        {
                            _randomArray[i] = randomNum;
                        }
                        else
                        {
                            _randomArray[i] = randomNum;
                            numberDoneList.Add(randomNum);
                        }
                        _isNumPlaced = true;
                    }
                    else if(numberDoneList.Count == _texture2Ds.Count)
                    {
                        break;
                    }
                }
            }
        }

        public async void SelectedCard(Card card, bool isFace)
        {
            if (GameState == GameState.None || card.IsMatched || isFace) return;
            card.ShowFace();
            AudioSystem.Instance.PlayMusic(AudioClips.Select);

            if (_waitSecondCard)
            {
                //check match
                if (IsMatch(_firstCard, card))
                {
                    _firstCard.IsMatched = true;
                    card.IsMatched = true;
                    _matchesQty++;
                    SetScoreText(_matchesQty);
                    AudioSystem.Instance.PlayMusic(AudioClips.Match);
                    if (_matchesQty >=3)
                    {
                        SessionDone();
                    }
                }
                else
                {
                    await UniTaskUtils.Delay(1);
                    _firstCard.ShowBackside();
                    card.ShowBackside();

                }
                _waitSecondCard = false;
                _firstCard = null;
            }
            else
            {
                _waitSecondCard = true;
                _firstCard = card;
            }
        }

        private bool IsMatch(Card a, Card b)
        {
            if (a.ImageNumber == b.ImageNumber)
            {
                return true;
            }
            return false;
        }
    }

    public enum GameState
    {
        Playing,
        None
    }
}