﻿using DG.Tweening;
using Game;
using UnityEngine;

public class Card : MonoBehaviour
{
   [SerializeField] private Renderer _faceRenderer;
   [SerializeField] private Renderer _backRenderer;

   public bool IsFace { get; set; }
   public bool IsMatched { get; set; }
   public int ImageNumber { get; set; }
   private void Start()
   {
      ShowBackside();
      IsFace = false;
   }

   public void SetBackTexture(Texture2D img)
   {
      _backRenderer.material.color = Color.white;
      _backRenderer.material.mainTexture = img;
   }
   
   public void SetFaceTexture(Texture2D img)
   {
      _faceRenderer.material.color = Color.white;
      _faceRenderer.material.mainTexture = img;
   }

   public void ShowFace()
   {
      transform.DOLocalRotate(new Vector3(0,0,0), 1, RotateMode.Fast);
      IsFace = true;
   }
   
   public void ShowBackside()
   {
      transform.DOLocalRotate(new Vector3(0,180,0), 1, RotateMode.Fast);
      IsFace = false;
   }

   public void OnMouseDown()
   {
      CardSelected();
   }

   public void CardSelected()
   {
      GameManager.Instance.SelectedCard(this, IsFace);
   }
}
