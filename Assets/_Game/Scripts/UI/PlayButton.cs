﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game.UI
{
   
    public class PlayButton : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private Image _sourceImage;
        [SerializeField] private Sprite _playImage;
        [SerializeField] private Sprite _pauseImage;

        private void Start()
        {
            GameManager.sessionDoneAction += OnSessionDone;
            _sourceImage.sprite = _playImage;
        }

        private void OnSessionDone()
        {
            _sourceImage.sprite = _playImage;
        }

        public void ButtonClick()
        {
            // if (GameManager.Instance.WaitTextureLoad) return;

            AudioSystem.Instance.PlayOneShot(AudioClips.ButtonClick);
            
            if( GameManager.Instance.GameState == GameState.Playing)
            {
                GameManager.Instance.GameState = GameState.None;
                _sourceImage.sprite = _playImage;

                GameManager.Instance.StopSession();
            } 
            else
            {
                GameManager.Instance.GameState = GameState.Playing;
                _sourceImage.sprite = _pauseImage;

                GameManager.Instance.StartNewSession();
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            // ButtonClick();
        }
    }

}